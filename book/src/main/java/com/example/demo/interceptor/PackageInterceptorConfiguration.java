package com.example.demo.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class PackageInterceptorConfiguration {
	@Bean
	public PackageInterceptor notifyAspect() {
		return new PackageInterceptor();
	}
}
