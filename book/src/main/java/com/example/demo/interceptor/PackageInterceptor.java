package com.example.demo.interceptor;

import java.lang.reflect.Method;
import java.util.logging.Level;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

@Aspect
public class PackageInterceptor {

    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(PackageInterceptor.class.getName());

    @After("execution(* com.fedex.airops.controller..*(..))")
    public void intercept( JoinPoint point) throws Throwable {
    	
        Method method = MethodSignature.class.cast(point.getSignature()).getMethod();
        String packageName = point.getSignature().getDeclaringType().getPackage().getName();
        String cName = method.getDeclaringClass().getSimpleName();
        String mName = method.getName();

        LOGGER.log(Level.INFO, "Entering {0}:{1}:{2}", new Object[]{packageName, cName, mName});        
        
    }   
}
